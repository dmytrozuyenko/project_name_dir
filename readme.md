# Project-Name-Dir

Docker Compose project of a LEMP for Wordpress deploy.
Test task from DevIT company.

## Description
Should be installed of the system: docker, docker-compose, make, git.
1. In Home directory create "project_name_dir" directory.
2. In "project_name_dir" create "source" directory and put latest wordpress in it.
3. Create docker-compose.yml. That shoud contain configuration of webserver (apache or nginx), php-fpm 8, mysql 8 (or mariadb)
4. Mount "source" directory to webserver and php-fpm containers.
5. Database container should be mounted by volume.
6. Create "docker" directory in "project_name_dir". And "php-fpm" directory within "docker".
7. In "php-fpm" directory create Dockerfile, that contains description of install and setting up all necessary php modules for wordpress. There should be two config files, for php and for php-fpm. Mount those config files into php-fpm container.
8. Create "web-srv" directory in "docker" directory. Put webserver config file into "web-srv" directory and mount it to "web-srv" container.
9. Create "db" directory in "docker" directory. Put db config file into "db" directory and mount it to "web-srv" container.<br>
Profect structure:<br>
-- project_name_dir<br>
--- source<br>
--- docker<br>
----- db<br>
-------- db.conf<br> 
----- php-fpm<br>
-------- Dockerfile<br>
-------- php.conf<br>
-------- php-fpm.conf<br>
----- web-srv<br>
-------- web-srv.conf<br>
Project should work. Wordpress - should be installed, without plug-ins and themes.
10. Put the project to git and give access to admin@devit.group

ps: Project is currently running on the server AWS EC2 at http://18.221.160.48:8000/ (down)
